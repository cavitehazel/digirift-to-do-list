import { ref, watch, onMounted } from "vue";

export function useTodo() {
  const tasks = ref([]);

  const getTasksFromLocalStorage = () => {
    try {
      const storedTasks = localStorage.getItem("tasks");
      return storedTasks ? JSON.parse(storedTasks) : [];
    } catch (error) {
      console.error("Error accessing localStorage:", error);
      return [];
    }
  };

  const saveTasksToLocalStorage = (tasks) => {
    try {
      localStorage.setItem("tasks", JSON.stringify(tasks));
    } catch (error) {
      console.error("Error accessing localStorage:", error);
    }
  };

  onMounted(() => {
    // Load tasks from localStorage when the component is mounted
    tasks.value = getTasksFromLocalStorage();
  });

  // Watch for changes in tasks and update localStorage accordingly
  watch(
    tasks,
    (newTasks) => {
      saveTasksToLocalStorage(newTasks);
    },
    { deep: true }
  );

  const addTask = (task) => {
    if (task) {
      tasks.value.push(task);
      return tasks.value;
    }
  };

  const removeTask = (taskId) => {
    tasks.value = tasks.value.filter((task) => task.id !== taskId);
  };

  const removeAllTasks = () => {
    tasks.value.splice(0, tasks.value.length);
  };

  const setDoneStatus = (index) => {
    tasks.value[index].status.done = !tasks.value[index].status.done;
  };

  return {
    tasks,
    addTask,
    removeTask,
    removeAllTasks,
    setDoneStatus,
  };
}
