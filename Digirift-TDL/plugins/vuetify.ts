// import this after install `@mdi/font` package
import "@mdi/font/css/materialdesignicons.css";

import "vuetify/styles";
import { createVuetify } from "vuetify";
import colors from "vuetify/util/colors";

export default defineNuxtPlugin((app) => {
  const vuetify = createVuetify({
    theme: {
      defaultTheme: "myCustomTheme",
      themes: {
        myCustomTheme: {
          dark: false,
          colors: {
            background: colors.teal.darken3,
            primary: colors.blue.darken2,
            secondary: colors.grey.lighten1,
            error: colors.red.darken2,
            success: colors.teal.accent4,
            info: "#2196F3",
            warning: "#FB8C00",
          },
        },
      },
    },
  });
  app.vueApp.use(vuetify);
});
